$Id: 

FEED TAG FILTER.MODULE
--------------------------------------------------------------------------------
This module is FeedAPI addon that filters feeditems bases on tags. When a feeditem contains a tag of these filtertags, 
there will be no new node created. (The tag_filter does a nice job but needs more work).There is administration screen 
    with a form and a checkbox list of all the tags of selected vocabularies where an admin can check or uncheck tags. 
	Selected tags will be submitted to a table and these will be the tags to filter out the feeds. (Default for all the feeds)



MAINTAINERS
--------------------------------------------------------------------------------
Tamer Zoubi - <tamerzg@gmail.com>

SPONSORED BY

--------------------------------------------------------------------------------
Prosite (http://www.prosite.be)